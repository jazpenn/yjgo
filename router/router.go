package router

import (
	"fmt"
	_ "scloud/app/controller/api"
	_ "scloud/app/controller/demo"
	"scloud/app/controller/hello"
	_ "scloud/app/controller/module"
	_ "scloud/app/controller/monitor"
	_ "scloud/app/controller/system"
	errorc "scloud/app/controller/system/error"
	"scloud/app/controller/system/index"
	_ "scloud/app/controller/tool"

	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

func init() {
	s := g.Server()
	// 某些浏览器直接请求favicon.ico文件，特别是产生404时
	//s.SetRewrite("/favicon.ico", "/resource/favicon.ico")
	// 加载登陆路由
	s.Group("/", func(group *ghttp.RouterGroup) {
		group.ALL("/", hello.Hello)
		group.ALL("/login", index.Login)
		group.ALL("/captchaImage", index.CaptchaImage)
		group.ALL("/checklogin", index.CheckLogin)
		group.ALL("/500", errorc.Error)
		group.ALL("/404", errorc.NotFound)
		group.ALL("/403", errorc.Unauth)

		group.Group("/", func(group *ghttp.RouterGroup) {
			group.ALL("/index", index.Index)
			group.ALL("/logout", index.Logout)
		})
	})

	// 加载框架路由
	s.Group("/system", func(group *ghttp.RouterGroup) {
		group.ALL("/main", index.Main)
		group.ALL("/switchSkin", index.SwitchSkin)
		group.ALL("/download", index.Download)
	})

	// webSocket
	s.BindHandler("/ws", func(r *ghttp.Request) {
		ws, err := r.WebSocket()
		if err != nil {
			fmt.Println(err)
			r.Exit()
		}
		for {
			msgType, msg, err := ws.ReadMessage()
			if err != nil {
				return
			}
			fmt.Println("WebSocket 收到消息", string(msg))
			if err = ws.WriteMessage(msgType, msg); err != nil {
				return
			}
		}
	})
}
