package icon

import (
	"scloud/app/service/utils/response"

	"github.com/gogf/gf/net/ghttp"
)

func Fontawesome(r *ghttp.Request) {
	response.BuildTpl(r, "demo/icon/fontawesome.html").WriteTplExtend()
}

func Glyphicons(r *ghttp.Request) {
	response.BuildTpl(r, "demo/icon/glyphicons.html").WriteTplExtend()
}
