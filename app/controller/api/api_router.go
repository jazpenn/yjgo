package api

import (
	"scloud/app/controller/api/login"
	"scloud/app/service/middleware"

	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

func init() {
	s := g.Server("api")
	// 不需要鉴权
	s.Group("/api/v1", func(group *ghttp.RouterGroup) {
		group.POST("/login", login.Login)
	})
	// 需要鉴权
	s.Group("/api/v1", func(group *ghttp.RouterGroup) {
		group.Middleware(middleware.JWT)
		group.GET("/index", login.Index)
	})
}
