package api

import (
	"scloud/app/service/utils/response"

	"github.com/gogf/gf/net/ghttp"
)

// @Summary api测试
// @Description api测试
// @Accept  json
// @Produce  json
// @Success 200 {object} model.CommonRes
// @Router /api/index [get]
func Index(r *ghttp.Request) {
	response.SucessResp(r).WriteJsonExit()
}
