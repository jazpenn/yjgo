package boot

import (
	"scloud/app/service/system/dict"
	"scloud/app/service/system/permission"

	"github.com/gogf/gf/os/gview"
)

func init() {
	gview.Instance().BindFuncMap(gview.FuncMap{
		"hasPermi":          permission.HasPermi,
		"getPermiButton":    permission.GetPermiButton,
		"getDictLabel":      dict.GetDictLabel,
		"getDictTypeSelect": dict.GetDictTypeSelect,
		"getDictTypeRadio":  dict.GetDictTypeRadio,
		"getDictTypeData":   dict.GetDictTypeData,
	})
}
