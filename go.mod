module scloud

require (
	github.com/StackExchange/wmi v0.0.0-20190523213315-cbe66965904d // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-ole/go-ole v1.2.4 // indirect
	github.com/gogf/gf v1.11.4
	github.com/lib/pq v1.3.0
	github.com/mojocn/base64Captcha v1.2.2
	github.com/mssola/user_agent v0.5.1
	github.com/shirou/gopsutil v2.19.12+incompatible
	github.com/stretchr/testify v1.4.0 // indirect
	github.com/tealeg/xlsx v1.0.5
	google.golang.org/appengine v1.6.5 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)

go 1.13
